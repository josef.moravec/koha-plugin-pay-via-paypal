/*
    [% 
        TOKENS = {
            home_breadcrumb="Home"
            your_payment_breadcrumb="Your Payment"
            error = "Error"
            error_title = "there was a problem processing your payment"
            PAYPAL_UNABLE_TO_CONNECT = "<p>Unable to connect to PayPal.</p><p>Please try again later.</p>"
            PAYPAL_ERROR_PROCESSING = "<p>Unable to verify payment.</p><p>Please contact the library to verify your payment.</p>"
            PAYPAL_ERROR_STARTING = "<p>Unable to start your payment.</p><p>Please try again later.</p>"
            return = "Return to fine details"
        } 
    %]
*/